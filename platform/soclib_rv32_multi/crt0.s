##
#  MIT License
#
#  Copyright (c) 2019 Cesar Fuguet Tortolero
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
##
##
#  @file   crt0.s
#  @author Cesar Fuguet Tortolero
##
.set MAX_NCORES, 16
.set STACK_SIZE_PER_CORE, 0x10000
.set STACK_SIZE, STACK_SIZE_PER_CORE*MAX_NCORES
.set RETRY_DELAY, (1 << 13)

.section .start,"ax",@progbits

.extern main

## initialize the BSS data section
.macro _init_bss rbegin, rend
	la       \rbegin, _ld_symbol_bss_begin
	la       \rend, _ld_symbol_bss_end
	beq      \rbegin, \rend, 2f
1:
	sw       zero, 0(\rbegin)
	addi     \rbegin, \rbegin, 4
	bne      \rbegin, \rend, 1b
2:
.endm

.globl _start
.align 2

_start:
	# get core ID
	csrr     t0, mhartid

	# each core computes its stack pointer
	li       t1, STACK_SIZE_PER_CORE
	addi     t2, t0, 1
	mul      t1, t1, t2
	la       sp, _stack_begin
	add      sp, sp, t1

	# thread dispatching according to its CPU number
	bne      t0, zero, _start_secondary

	# core 0 initializes the BSS data section
	_init_bss t1, t2

	# core 0 goes to the main function
	jal      main
	j        _start_endloop

_start_secondary:
#if MAX_NCORES > 1
	# other cores wait until their boot vector entry is modified
	la       t1, _cpu_boot_vector
	slli     t2, t0, 2
	add      t1, t1, t2
1:
	lw       t2, 0(t1)
	bne      t2, zero, 3f

	# wait some cycles before retrying (reduce memory contention)
	li       t2, RETRY_DELAY
2:
	addi     t2, t2, -1
	bne      t2, zero, 2b
	j        1b

3:
	# secondary cores jump to the address read from the boot vector
	jalr     t2
#endif

_start_endloop:
	j        _start_endloop


.section .stack, "aw",@progbits

.align 2

_stack_begin:
	.space   STACK_SIZE
_stack_end:
	.word    0xdeadbeef


.section .data

.globl _cpu_boot_vector
.align 2

_cpu_boot_vector:
	.rept   MAX_NCORES
	.word   0x0
	.endr
