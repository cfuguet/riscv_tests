##
#  MIT License
#
#  Copyright (c) 2019 Cesar Fuguet Tortolero
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
##
##
#  @file   compiler.mk
#  @author Cesar Fuguet Tortolero
##
CC_PREFIX ?= riscv32-unknown-elf-
CC        := $(CC_PREFIX)gcc
AS        := $(CC_PREFIX)as
LD        := $(CC_PREFIX)gcc
OD        := $(CC_PREFIX)objdump -Mno-aliases
SZ        := $(CC_PREFIX)size
AR        := $(CC_PREFIX)ar
Q         ?= @
ECHO      ?= @echo

LDSCRIPT  := ../../platform/$(PLATFORM)/linkcmds
INCLUDES  := -I../../common -I../../platform/$(PLATFORM)
LDPATHS   := -L../../common -L../../platform/$(PLATFORM)

CPUFLAGS  := -march=rv32imafc -mabi=ilp32f
CFLAGS    := $(CPUFLAGS) $(INCLUDES) -mcmodel=medany -nostdlib -ffreestanding
ASFLAGS   := $(CPUFLAGS) $(INCLUDES)
LDFLAGS   := -static -nostdlib -Wl,-EL $(LDPATHS) -T$(LDSCRIPT)

SRCS      += platform_atexit.c platform_tty.c crt0.s
