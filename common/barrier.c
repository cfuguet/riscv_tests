/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   barrier.c
 *  @author Cesar Fuguet Tortolero
 */
#include <barrier.h>

void barrier_init(barrier_t *barrier, int expected)
{
	barrier->expected = expected - 1;
	cpu_atomic_store(&barrier->count, 0);
	cpu_atomic_store(&barrier->sense, 0);
}

void barrier_wait(barrier_t *barrier)
{
	int sense = cpu_atomic_load(&barrier->sense);
	int count = cpu_atomic_fetch_add(&barrier->count, 1);
	int expected = barrier->expected - 1;

	if (count == expected) {
		cpu_atomic_store(&barrier->count, 0);
		cpu_atomic_store(&barrier->sense, !sense);
		return;
	}

	while (cpu_atomic_load(&barrier->sense) == sense);
}
