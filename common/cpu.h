/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   cpu.h
 *  @author Cesar Fuguet Tortolero
 */
#ifndef __CPUASM_H__
#define __CPUASM_H__

#include <stdint.h>

#define __cacheblock_aligned__ __attribute__ ((aligned (64)))

#define CPU_ENABLE_AMO

/* memory barrier operations */
static inline void cpu_dfence();
static inline void cpu_ifence();
static inline void cpu_mfence();

/* miscellaneous */
static inline int cpu_id();
static inline void cpu_delay(int ncycles);
static inline uint64_t cpu_cycles();
static inline uint64_t cpu_instructions();

/* atomic operations */
static inline int cpu_atomic_exchange(volatile int *obj, volatile int val);
static inline int cpu_atomic_fetch_add(volatile int *obj, volatile int val);
static inline int cpu_atomic_fetch_or(volatile int *obj, volatile int val);
static inline int cpu_atomic_fetch_and(volatile int *obj, volatile int val);
static inline int cpu_atomic_fetch_xor(volatile int *obj, volatile int val);
static inline void cpu_atomic_store(volatile int *obj, volatile int val);
static inline int cpu_atomic_load(volatile int *obj);

static inline void cpu_dfence()
{
	asm volatile ("fence  rw, rw\n" ::: "memory");
}

static inline void cpu_ifence()
{
	asm volatile ("fence.i\n" ::: "memory");
}

static inline void cpu_mfence()
{
	asm volatile ("" ::: "memory");
}

static inline int cpu_id()
{
	register int ret;
	asm volatile ("csrr %0, mhartid\n" : "=r"(ret));
	return ret;
}

static inline void cpu_delay(int ncycles)
{
	register int niters = ncycles / 3;

	asm volatile (
			"1:                                 \n"
			"beq    %[n], zero, 2f              \n"
			"addi   %[n], %[n], -1              \n"
			"j      1b                          \n"
			"2:                                 \n"
			: [n] "+r"(niters)
			:
			: "memory");
}

static inline uint64_t cpu_cycles()
{
	register uint32_t lo;
	register uint32_t hi;

	asm volatile (
			"csrr %[lo], mcycle                  \n"
			"csrr %[hi], mcycleh                 \n"
			: [lo] "=r"(lo), [hi] "=r"(hi)
			:
			: "memory");

	/* as there is one cycle between the reading of the lower bits and
	 * higher bits, decrement hi if lo was about to wrap */
	if (lo == 0xffffffffUL)
		hi--;

	return ((uint64_t)hi << 32) | lo;
}

static inline uint64_t cpu_instructions()
{
	register uint32_t lo;
	register uint32_t hi;

	asm volatile (
			"csrr %[lo], minstret                \n"
			"csrr %[hi], minstreth               \n"
			: [lo] "=r"(lo), [hi] "=r"(hi)
			:
			: "memory");

	/* as there is one cycle between the reading of the lower bits and
	 * higher bits, decrement hi if lo was about to wrap */
	if (lo == 0xffffffffUL)
		hi--;

	return ((uint64_t)hi << 32) | lo;
}

static inline int cpu_atomic_exchange(volatile int *obj, volatile int val)
{
	register int ret;
	register int tmp;

	(void)tmp;

#ifdef CPU_ENABLE_AMO
	asm volatile (  "amoswap.w %[ret], %[val], (%[obj])   \n"
			: [ret] "=r"(ret)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#else
	asm volatile (  "1:                                  \n"
			"lr.w    %[ret], (%[obj])            \n"
			"sc.w    %[tmp], %[val], (%[obj])    \n"
			"bne     %[tmp], zero, 1b            \n"
			: [ret] "=&r"(ret)
			, [tmp] "=&r"(tmp)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#endif

	return ret;
}

static inline int cpu_atomic_fetch_add(volatile int *obj, volatile int val)
{
	register int ret;
	register int tmp;

	(void)tmp;

#ifdef CPU_ENABLE_AMO
	asm volatile (  "amoadd.w %[ret], %[val], (%[obj])   \n"
			: [ret] "=r"(ret)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#else
	asm volatile (  "1:                                  \n"
			"lr.w    %[ret], (%[obj])            \n"
			"add     %[tmp], %[ret], %[val]      \n"
			"sc.w    %[tmp], %[tmp], (%[obj])    \n"
			"bne     %[tmp], zero, 1b            \n"
			: [ret] "=&r"(ret)
			, [tmp] "=&r"(tmp)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#endif

	return ret;
}

static inline int cpu_atomic_fetch_or(volatile int *obj, volatile int val)
{
	register int ret;
	register int tmp;

	(void)tmp;

#ifdef CPU_ENABLE_AMO
	asm volatile (  "amoor.w %[ret], %[val], (%[obj])   \n"
			: [ret] "=r"(ret)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#else
	asm volatile (  "1:                                  \n"
			"lr.w    %[ret], (%[obj])            \n"
			"or      %[tmp], %[ret], %[val]      \n"
			"sc.w    %[tmp], %[tmp], (%[obj])    \n"
			"bne     %[tmp], zero, 1b            \n"
			: [ret] "=&r"(ret)
			, [tmp] "=&r"(tmp)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#endif

	return ret;
}

static inline int cpu_atomic_fetch_and(volatile int *obj, volatile int val)
{
	register int ret;
	register int tmp;

	(void)tmp;

#ifdef CPU_ENABLE_AMO
	asm volatile (  "amoand.w %[ret], %[val], (%[obj])   \n"
			: [ret] "=r"(ret)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#else
	asm volatile (  "1:                                  \n"
			"lr.w    %[ret], (%[obj])            \n"
			"and     %[tmp], %[ret], %[val]      \n"
			"sc.w    %[tmp], %[tmp], (%[obj])    \n"
			"bne     %[tmp], zero, 1b            \n"
			: [ret] "=&r"(ret)
			, [tmp] "=&r"(tmp)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#endif

	return ret;
}

static inline int cpu_atomic_fetch_xor(volatile int *obj, volatile int val)
{
	register int ret;
	register int tmp;

	(void)tmp;

#ifdef CPU_ENABLE_AMO
	asm volatile (  "amoxor.w %[ret], %[val], (%[obj])   \n"
			: [ret] "=r"(ret)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#else
	asm volatile (  "1:                                  \n"
			"lr.w    %[ret], (%[obj])            \n"
			"xor     %[tmp], %[ret], %[val]      \n"
			"sc.w    %[tmp], %[tmp], (%[obj])    \n"
			"bne     %[tmp], zero, 1b            \n"
			: [ret] "=&r"(ret)
			, [tmp] "=&r"(tmp)
			: [obj] "r"(obj)
			, [val] "r"(val)
			: "memory");
#endif

	return ret;
}

static inline void cpu_atomic_store(volatile int *obj, volatile int val)
{
	*obj = val;
	cpu_dfence();
}

static inline int cpu_atomic_load(volatile int *obj)
{
	return *obj;
}

#endif /* __CPU_H__ */
