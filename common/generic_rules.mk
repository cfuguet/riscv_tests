##
#  MIT License
#
#  Copyright (c) 2019 Cesar Fuguet Tortolero
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
##
##
#  @file   generic_rules.mk
#  @author Cesar Fuguet Tortolero
##
vpath %.S ../../common ../../platform/$(PLATFORM)
vpath %.s ../../common ../../platform/$(PLATFORM)
vpath %.c ../../common ../../platform/$(PLATFORM)

OBJS = \
$(subst .s,.s.o,$(filter %.s,$(SRCS))) \
$(subst .S,.S.o,$(filter %.S,$(SRCS))) \
$(subst .c,.c.o,$(filter %.c,$(SRCS)))

COMMON_OBJS = stdio.c.o string.c.o stdlib.c.o printf.c.o

LIBCOMMON = common

LDFLAGS := -L. $(LDFLAGS)
LIBS := -l$(LIBCOMMON) $(LIBS)

.PHONY: all
all: $(TRGT) $(TRGT).dump $(TRGT).size

$(TRGT): lib$(LIBCOMMON).a $(OBJS)
	$(ECHO) "Build executable: $@"
	$(Q)$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

$(TRGT).dump: $(TRGT)
	$(Q)$(OD) -D $(TRGT) > $@

$(TRGT).size: $(TRGT)
	$(Q)$(SZ) $(TRGT) > $@

lib$(LIBCOMMON).a: $(COMMON_OBJS)
	$(ECHO) "Build library: $@"
	$(Q)$(AR) rs $@ $^ 2>/dev/null


.PHONY: clean
clean:
	$(ECHO) "Clean all object files"
	$(Q)rm -f $(OBJS) $(COMMON_OBJS) lib$(LIBCOMMON).a
	$(Q)rm -f $(TRGT) $(TRGT).dump $(TRGT).size

%.s.o: %.s
	$(ECHO) "Build object: $@"
	$(Q)$(AS) $(ASFLAGS) -o $@ $<

%.c.o: %.c
	$(ECHO) "Build object: $@"
	$(Q)$(CC) $(CFLAGS) -c -o $@ $<

%.S.o: %.S
	$(ECHO) "Build object: $@"
	$(Q)$(CC) $(CFLAGS) -c -o $@ $<
