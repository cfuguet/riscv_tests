/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   stdio.c
 *  @author Cesar Fuguet Tortolero
 */
#include "stdio.h"
#include "platform_tty.h"

void puts(char *s)
{
	while (*s != 0)
		putchar(*s++);
}

void putu(unsigned d)
{
	char buf[11];
	char *s = &buf[10];

	*s-- = 0;
	do {
		int i = d % 10;
		*s-- = '0' + i;
		d /= 10;
	} while (d != 0);

	puts(s + 1);
}

void putd(int d)
{
	if (d < 0) {
		d = -d;
		putchar('-');
	}
	putu(d);
}

void putx(unsigned d)
{
	static char tohex[] = "0123456789abcdef";
	char buf[9];
	char *s = &buf[8];

	*s-- = 0;
	do {
		int i = d & 0xf;
		*s-- = tohex[i];
		d >>= 4;
	} while (d != 0);

	puts("0x");
	puts(s + 1);
}
