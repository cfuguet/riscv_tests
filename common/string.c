/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   string.c
 *  @author Cesar Fuguet Tortolero
 */
#include "string.h"
#include <stdint.h>

static inline void* __memset_byte(
		unsigned char *s,
		unsigned char c,
		size_t nbytes)
{
	for (; nbytes > 0; nbytes--)
		*s++ = c;

	return (void*)s;
}

static inline void* __memset_word(
		unsigned long *s,
		unsigned char c,
		size_t nwords)
{
	unsigned long word = (c << 24) | (c << 16) | (c << 8) | c;

#if __SIZEOF_LONG__ == 8
	word = (word << 32) | word;
#endif

	for (; nwords > 0; nwords--)
		*s++ = word;

	return (void*)s;
}

void *memset(void *s, int c, size_t n)
{
	const void *_s = s;
	const size_t wsize = sizeof(long);

	if ((((uintptr_t)s % wsize) != 0) && (n >= wsize)) {
		size_t nbytes = wsize - ((uintptr_t)s % wsize);
		s = __memset_byte((unsigned char*)s, (unsigned char)c,
				nbytes);
		n -= nbytes;
	}

	if (n >= wsize) {
		s = __memset_word((unsigned long*)s, (unsigned char)c,
				n / wsize);
		n %= wsize;
	}

	if (n > 0)
		__memset_byte((unsigned char*)s, (unsigned char)c, n);

	return (void*)_s;
}

void *memchr(const void *s, int c, size_t n)
{
	const unsigned char *_s = s;
	unsigned char _c = c;

	for (; n > 0; n--, _s++) {
		if (*_s == _c)
			return (void*)_s;
	}

	return NULL;
}

static inline void* __memcpy_bytes(
		unsigned char *dest,
		const unsigned char *src,
		size_t nbytes)
{
	for (; nbytes > 0; nbytes--)
		*dest++ = *src++;

	return (void*)dest;
}

static inline void* __memcpy_words(
		unsigned long *dest,
		const unsigned long *src,
		size_t nwords)
{
	for (; nwords > 0; nwords--)
		*dest++ = *src++;

	return (void*)dest;
}

void *memcpy(void *dest, const void *src, size_t n)
{
	const void *_s = src;
	void *_d = dest;
	const size_t wsize = sizeof(long);

	if ((((uintptr_t)_s % wsize) != 0) && (n >= wsize)) {
		size_t nbytes = wsize - ((uintptr_t)_s % wsize);
		_d = __memcpy_bytes((unsigned char*)_d, (unsigned char*)_s,
				nbytes);
		_s = (unsigned char*)_s + nbytes;
		n -= nbytes;
	}

	if (n >= wsize) {
		size_t nwords = n / wsize;
		_d = __memcpy_words((unsigned long*)_d, (unsigned long*)_s,
				nwords);
		_s = (unsigned long*)_s + nwords;
		n %= wsize;
	}

	if (n > 0)
		__memcpy_bytes((unsigned char*)_d, (unsigned char*)_s, n);

	return dest;
}

int memcmp(const void *s1, const void *s2, size_t n)
{
	size_t i;
	const unsigned char *_s1 = s1;
	const unsigned char *_s2 = s2;

	for (i = 0; i < n; i++) {
		if (_s1[i] < _s2[i])
			return _s2[i] - _s1[i];
		if (_s2[i] < _s1[i])
			return _s1[i] - _s2[i];
	}

	return 0;
}

void *memmove(void *dest, const void *src, size_t n)
{
	const unsigned char *_s;
	unsigned char *_d;

	/* Nothing to move */
	if (dest == src)
		return dest;

	/* Source and destination buffers do not overlap */
	if ((dest < src) || ((size_t)dest >= ((size_t)src + n)))
		return memcpy(dest, src, n);

	/* Source and destination buffers overlap. Fill from the end to the
	 * beginning to avoid any data destruction. */
	_s = (unsigned char*)src + (n - 1);
	_d = (unsigned char*)dest + (n - 1);
	for (; n > 0; n--)
		*_d-- = *_s--;

	return dest;
}

char* strcpy(char *dst, const char *src)
{
	char *const ret = dst;

	while (*src != '\0')
		*dst++ = *src++;

	*dst = '\0';

	return ret;
}

char* strncpy(char *dst, const char *src, size_t n)
{
	char *const ret = dst;
	size_t i;

	for (i = 0; (i < n) && (*src != '\0'); i++)
		*dst++ = *src++;
	for (; i < n; i++)
		*dst++ = '\0';

	return ret;
}

size_t strlen(const char *s)
{
	const char *_s = s;

	while (*_s != '\0')
		_s++;

	return (size_t)_s - (size_t)s;
}

char* strcat(char *s1, const char *s2)
{
	char *const ret = s1;

	/* go the end of the s1 string */
	while (*s1 != '\0')
		s1++;

	/* concat characters */
	while (*s2 != '\0')
		*s1++ = *s2++;

	*s1 = '\0';

	return ret;
}

char* strncat(char *s1, const char *s2, size_t n)
{
	char *const ret = s1;
	size_t i;

	/* go the end of the s1 string */
	while (*s1 != '\0')
		s1++;

	/* concat at most 'n' characters */
	for (i = 0; (i < n) && (*s2 != '\0'); i++)
		*s1++ = *s2++;

	*s1 = '\0';

	return ret;
}

int strcmp(const char *s1, const char *s2)
{
	while ((*s1 != '\0') && (*s1 == *s2)) {
		s1++;
		s2++;
	}

	return (int)*s1 - (int)*s2;
}

int strncmp(const char *s1, const char *s2, size_t n)
{
	size_t i = 0;

	while ((*s1 != '\0') && (*s1 == *s2) && (i++ < n)) {
		s1++;
		s2++;
	}

	return (int)*s1 - (int)*s2;
}
