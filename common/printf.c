/*
 * Copyright (c) 2012-2014 ARM Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the company may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ARM LTD ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ARM LTD BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   printf.c
 *  @author Cesar Fuguet Tortolero
 */
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>

#define _ATTRIBUTE __attribute__
#define NO_FLOATING_POINT

#ifndef NO_FLOATING_POINT
# define FLOATING_POINT
#endif

#define _PRINTF_FLOAT_TYPE double

#if defined (FLOATING_POINT)
# include <locale.h>
#endif
#ifdef FLOATING_POINT
# include <math.h>

/* For %La, an exponent of 15 bits occupies the exponent character,
   a sign, and up to 5 digits.  */
# define MAXEXPLEN		7
# define DEFPREC		6

extern char *_dtoa_r (struct *, double, int,
		int, int *, int *, char **);

# define _DTOA_R _dtoa_r
# define FREXP frexp

#endif /* FLOATING_POINT.  */

/* BUF must be big enough for the maximum %#llo (assuming long long is
   at most 64 bits, this would be 23 characters), the maximum
   multibyte character %C, and the maximum default precision of %La
   (assuming long double is at most 128 bits with 113 bits of
   mantissa, this would be 29 characters).  %e, %f, and %g use
   reentrant storage shared with mprec.  All other formats that use
   buf get by with fewer characters.  Making BUF slightly bigger
   reduces the need for malloc in %.*a and %S, when large precision or
   long strings are processed.
   The bigger size of 100 bytes is used on systems which allow number
   strings using the locale's grouping character.  Since that's a multibyte
   value, we should use a conservative value.  */
#define BUF		40

typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;
typedef long quad_t;
typedef unsigned long u_quad_t;

typedef quad_t * quad_ptr_t;
typedef void *void_ptr_t;
typedef char *   char_ptr_t;
typedef long *   long_ptr_t;
typedef int  *   int_ptr_t;
typedef short *  short_ptr_t;

/* Macros for converting digits to letters and vice versa.  */
#define to_digit(c)	((c) - '0')
#define is_digit(c)	((unsigned)to_digit (c) <= 9)
#define to_char(n)	((n) + '0')

/* Flags used during conversion.  */
#define ALT		0x001		/* Alternate form.  */
#define LADJUST		0x002		/* Left adjustment.  */
#define ZEROPAD		0x004		/* Zero (as opposed to blank) pad.  */
#define PLUSSGN		0x008		/* Plus sign flag.  */
#define SPACESGN	0x010		/* Space flag.  */
#define HEXPREFIX	0x020		/* Add 0x or 0X prefix.  */
#define SHORTINT	0x040		/* Short integer.  */
#define LONGINT		0x080		/* Long integer.  */
#define LONGDBL		0x100		/* Long double.  */
/* ifdef _NO_LONGLONG, make QUADINT equivalent to LONGINT, so
   that %lld behaves the same as %ld, not as %d, as expected if:
   sizeof (long long) = sizeof long > sizeof int.  */
#define QUADINT		LONGINT
#define FPT		0x400		/* Floating point number.  */
/* Define as 0, to make SARG and UARG occupy fewer instructions.  */
# define CHARINT	0

/* Macros to support positional arguments.  */
#define GET_ARG(n, ap, type) (va_arg ((ap), type))

/* To extend shorts properly, we need both signed and unsigned
   argument extraction methods.  Also they should be used in nano-vfprintf_i.c
   and nano-vfprintf_float.c only, since ap is a pointer to va_list.  */
#define SARG(flags) \
	(flags&LONGINT ? GET_ARG (N, (*ap), long) : \
	 flags&SHORTINT ? (long)(short)GET_ARG (N, (*ap), int) : \
	 flags&CHARINT ? (long)(signed char)GET_ARG (N, (*ap), int) : \
	 (long)GET_ARG (N, (*ap), int))
#define UARG(flags) \
	(flags&LONGINT ? GET_ARG (N, (*ap), u_long) : \
	 flags&SHORTINT ? (u_long)(u_short)GET_ARG (N, (*ap), int) : \
	 flags&CHARINT ? (u_long)(unsigned char)GET_ARG (N, (*ap), int) : \
	 (u_long)GET_ARG (N, (*ap), u_int))

static inline void PRINT(const char *str, int n)
{
	int i;
	for (i = 0; i < n; i++)
		putchar(str[i]);
}

#define PAD(howmany, ch) {              \
	int temp_i = 0;                 \
	while (temp_i < (howmany))      \
	{                               \
		PRINT (&(ch), 1);       \
		temp_i++;               \
	}                               \
}
#define PRINTANDPAD(p, ep, len, ch) {   \
	int temp_n = (ep) - (p);        \
	if (temp_n > (len))             \
		temp_n = (len);         \
	if (temp_n > 0)                 \
		PRINT((p), temp_n);     \
	PAD((len) - (temp_n > 0 ? temp_n : 0), (ch)); \
}

/* All data needed to decode format string are kept in below struct.  */
struct _prt_data_t
{
	int flags;		/* Flags.  */
	int prec;		/* Precision.  */
	int dprec;		/* Decimal precision.  */
	int width;		/* Width.  */
	int size;		/* Size of converted field or string.  */
	int ret;		/* Return value accumulator.  */
	char code;		/* Current conversion specifier.  */
	char blank;		/* Blank character.  */
	char zero;		/* Zero character.  */
	char buf[BUF];	/* Output buffer for non-floating point number.  */
	char l_buf[3];	/* Sign&hex_prefix, "+/-" and "0x/X".  */
#ifdef FLOATING_POINT
	_PRINTF_FLOAT_TYPE _double_;	/* Double value.  */
	char expstr[MAXEXPLEN];	/* Buffer for exponent string.  */
	int lead;		/* The sig figs before decimal or group sep.  */
#endif
};

int _printf_common(struct _prt_data_t *pdata, int *realsz);
int _printf_i(struct _prt_data_t *pdata, va_list *ap);
int _printf_float (struct _prt_data_t *pdata, va_list *ap) _ATTRIBUTE((__weak__));
int printf (const char *__restrict fmt, ...);
int vfprintf(const char *fmt0, va_list ap);

int printf (const char *__restrict fmt, ...)
{
	int ret;
	va_list ap;

	va_start (ap, fmt);
	ret = vfprintf(fmt, ap);
	va_end (ap);
	return ret;
}

int vfprintf(const char *fmt0, va_list ap)
{
	register char *fmt;   /* Format string.  */
	register int n, m;    /* Handy integers (short term usage).  */
	register char *cp;    /* Handy char pointer (short term usage).  */
	const char *flag_chars;
	struct _prt_data_t prt_data;  /* All data for decoding format string.  */
	va_list ap_copy;

	fmt = (char *)fmt0;
	prt_data.ret = 0;
	prt_data.blank = ' ';
	prt_data.zero = '0';

	/* GCC PR 14577 at https://gcc.gnu.org/bugzilla/show_bug.cgi?id=14557 */
	va_copy (ap_copy, ap);

	/* Scan the format for conversions (`%' character).  */
	for (;;)
	{
		cp = fmt;
		while (*fmt != '\0' && *fmt != '%')
			fmt += 1;

		if ((m = fmt - cp) != 0)
		{
			PRINT (cp, m);
			prt_data.ret += m;
		}
		if (*fmt == '\0')
			goto done;

		fmt++;            /* Skip over '%'.  */

		prt_data.flags = 0;
		prt_data.width = 0;
		prt_data.prec = -1;
		prt_data.dprec = 0;
		prt_data.l_buf[0] = '\0';
#ifdef FLOATING_POINT
		prt_data.lead = 0;
#endif
		/* The flags.  */
		/*
		 * ``Note that 0 is taken as a flag, not as the
		 * beginning of a field width.''
		 *        -- ANSI X3J11
		 */
		flag_chars = "#-0+ ";
		for (; (cp = memchr (flag_chars, *fmt, 5)); fmt++)
			prt_data.flags |= (1 << (cp - flag_chars));

		if (prt_data.flags & SPACESGN)
			prt_data.l_buf[0] = ' ';

		/*
		 * ``If the space and + flags both appear, the space
		 * flag will be ignored.''
		 *        -- ANSI X3J11
		 */
		if (prt_data.flags & PLUSSGN)
			prt_data.l_buf[0] = '+';

		/* The width.  */
		if (*fmt == '*')
		{
			/*
			 * ``A negative field width argument is taken as a
			 * - flag followed by a positive field width.''
			 *    -- ANSI X3J11
			 * They don't exclude field widths read from args.
			 */
			prt_data.width = GET_ARG (n, ap_copy, int);
			if (prt_data.width < 0)
			{
				prt_data.width = -prt_data.width;
				prt_data.flags |= LADJUST;
			}
			fmt++;
		}
		else
		{
			for (; is_digit (*fmt); fmt++)
				prt_data.width = 10 * prt_data.width + to_digit (*fmt);
		}

		/* The precision.  */
		if (*fmt == '.')
		{
			fmt++;
			if (*fmt == '*')
			{
				fmt++;
				prt_data.prec = GET_ARG (n, ap_copy, int);
				if (prt_data.prec < 0)
					prt_data.prec = -1;
			}
			else
			{
				prt_data.prec = 0;
				for (; is_digit (*fmt); fmt++)
					prt_data.prec = 10 * prt_data.prec + to_digit (*fmt);
			}
		}

		/* The length modifiers.  */
		flag_chars = "hlL";
		if ((cp = memchr (flag_chars, *fmt, 3)) != NULL)
		{
			prt_data.flags |= (SHORTINT << (cp - flag_chars));
			fmt++;
		}

		/* The conversion specifiers.  */
		prt_data.code = *fmt++;
		cp = memchr ("efgEFG", prt_data.code, 6);
#ifdef FLOATING_POINT
		/* If cp is not NULL, we are facing FLOATING POINT NUMBER.  */
		if (cp)
		{
			/* Consume floating point argument if _printf_float is not
			   linked.  */
			if (_printf_float == NULL)
			{
				if (prt_data.flags & LONGDBL)
					GET_ARG (N, ap_copy, _LONG_DOUBLE);
				else
					GET_ARG (N, ap_copy, double);
			}
			else
				n = _printf_float (&prt_data, &ap_copy);
		}
		else
#endif
			n = _printf_i(&prt_data, &ap_copy);

		if (n == -1)
			goto error;

		prt_data.ret += n;
	}
done:
error:
	va_end (ap_copy);
	return prt_data.ret;
}

/* Decode and print non-floating point data.  */
int _printf_common(struct _prt_data_t *pdata, int *realsz)
{
	int n;
	/*
	 * All reasonable formats wind up here.  At this point, `cp'
	 * points to a string which (if not flags&LADJUST) should be
	 * padded out to `width' places.  If flags&ZEROPAD, it should
	 * first be prefixed by any sign or other prefix; otherwise,
	 * it should be blank padded before the prefix is emitted.
	 * After any left-hand padding and prefixing, emit zeroes
	 * required by a decimal [diouxX] precision, then print the
	 * string proper, then emit zeroes required by any leftover
	 * floating precision; finally, if LADJUST, pad with blanks.
	 * If flags&FPT, ch must be in [aAeEfg].
	 *
	 * Compute actual size, so we know how much to pad.
	 * size excludes decimal prec; realsz includes it.
	 */
	*realsz = pdata->dprec > pdata->size ? pdata->dprec : pdata->size;
	if (pdata->l_buf[0])
		(*realsz)++;

	if (pdata->flags & HEXPREFIX)
		*realsz += 2;

	/* Right-adjusting blank padding.  */
	if ((pdata->flags & (LADJUST|ZEROPAD)) == 0)
		PAD (pdata->width - *realsz, pdata->blank);

	/* Prefix.  */
	n = 0;
	if (pdata->l_buf[0])
		n++;

	if (pdata->flags & HEXPREFIX)
	{
		pdata->l_buf[n++] = '0';
		pdata->l_buf[n++] = pdata->l_buf[2];
	}

	PRINT (pdata->l_buf, n);
	n = pdata->width - *realsz;
	if ((pdata->flags & (LADJUST|ZEROPAD)) != ZEROPAD || n < 0)
		n = 0;

	if (pdata->dprec > pdata->size)
		n += pdata->dprec - pdata->size;

	PAD (n, pdata->zero);
	return 0;
}

int _printf_i(struct _prt_data_t *pdata, va_list *ap)
{
	/* Field size expanded by dprec.  */
	int realsz;
	u_quad_t _uquad;
	int base;
	int n;
	char *cp = pdata->buf + BUF;
	char *xdigs = "0123456789ABCDEF";

	/* Decoding the conversion specifier.  */
	switch (pdata->code)
	{
		case 'c':
			*--cp = GET_ARG (N, *ap, int);
			pdata->size = 1;
			goto non_number_nosign;
		case 'd':
		case 'i':
			_uquad = SARG (pdata->flags);
			if ((long) _uquad < 0)
			{
				_uquad = -_uquad;
				pdata->l_buf[0] = '-';
			}
			base = 10;
			goto number;
		case 'u':
		case 'o':
			_uquad = UARG (pdata->flags);
			base = (pdata->code == 'o') ? 8 : 10;
			goto nosign;
		case 'X':
			pdata->l_buf[2] = 'X';
			goto hex;
		case 'p':
			/*
			 * ``The argument shall be a pointer to void.  The
			 * value of the pointer is converted to a sequence
			 * of printable characters, in an implementation-
			 * defined manner.''
			 *	-- ANSI X3J11
			 */
			pdata->flags |= HEXPREFIX;
			if (sizeof (void*) > sizeof (int))
				pdata->flags |= LONGINT;
			/* NOSTRICT.  */
		case 'x':
			pdata->l_buf[2] = 'x';
			xdigs = "0123456789abcdef";
hex:
			_uquad = UARG (pdata->flags);
			base = 16;
			if (pdata->flags & ALT)
				pdata->flags |= HEXPREFIX;

			/* Leading 0x/X only if non-zero.  */
			if (_uquad == 0)
				pdata->flags &= ~HEXPREFIX;

			/* Unsigned conversions.  */
nosign:
			pdata->l_buf[0] = '\0';
			/*
			 * ``... diouXx conversions ... if a precision is
			 * specified, the 0 flag will be ignored.''
			 *	-- ANSI X3J11
			 */
number:
			if ((pdata->dprec = pdata->prec) >= 0)
				pdata->flags &= ~ZEROPAD;

			/*
			 * ``The result of converting a zero value with an
			 * explicit precision of zero is no characters.''
			 *	-- ANSI X3J11
			 */
			if (_uquad != 0 || pdata->prec != 0)
			{
				do
				{
					*--cp = xdigs[_uquad % base];
					_uquad /= base;
				}
				while (_uquad);
			}
			/* For 'o' conversion, '#' increases the precision to force the first
			   digit of the result to be zero.  */
			if (base == 8 && (pdata->flags & ALT) && pdata->prec <= pdata->size)
				*--cp = '0';

			pdata->size = pdata->buf + BUF - cp;
			break;
		case 'n':
			if (pdata->flags & LONGINT)
				*GET_ARG (N, *ap, long_ptr_t) = pdata->ret;
			else if (pdata->flags & SHORTINT)
				*GET_ARG (N, *ap, short_ptr_t) = pdata->ret;
			else
				*GET_ARG (N, *ap, int_ptr_t) = pdata->ret;
		case '\0':
			pdata->size = 0;
			break;
		case 's':
			cp = GET_ARG (N, *ap, char_ptr_t);
			/* Precision gives the maximum number of chars to be written from a
			   string, and take prec == -1 into consideration.
			   Use normal Newlib approach here to support case where cp is not
			   nul-terminated.  */
			char *p = memchr (cp, 0, pdata->prec);

			if (p != NULL)
				pdata->prec = p - cp;

			pdata->size = pdata->prec;
			goto non_number_nosign;
		default:
			/* "%?" prints ?, unless ? is NUL.  */
			/* Pretend it was %c with argument ch.  */
			*--cp = pdata->code;
			pdata->size = 1;
non_number_nosign:
			pdata->l_buf[0] = '\0';
			break;
	}

	/* Output.  */
	n = _printf_common (pdata, &realsz);
	if (n == -1)
		goto error;

	PRINT (cp, pdata->size);
	/* Left-adjusting padding (always blank).  */
	if (pdata->flags & LADJUST)
		PAD (pdata->width - realsz, pdata->blank);

	return (pdata->width > realsz ? pdata->width : realsz);
error:
	return -1;
}

char *__cvt (_PRINTF_FLOAT_TYPE value, int ndigits, int flags, char *sign,
		int *decpt, int ch, int *length, char *buf);

int __exponent (char *p0, int exp, int fmtch);

#ifdef FLOATING_POINT

/* Using reentrant DATA, convert finite VALUE into a string of digits
   with no decimal point, using NDIGITS precision and FLAGS as guides
   to whether trailing zeros must be included.  Set *SIGN to nonzero
   if VALUE was negative.  Set *DECPT to the exponent plus one.  Set
 *LENGTH to the length of the returned string.  CH must be one of
 [aAeEfFgG]; if it is [aA], then the return string lives in BUF,
 otherwise the return value shares the mprec reentrant storage.  */
char *__cvt (_PRINTF_FLOAT_TYPE value, int ndigits, int flags, char *sign,
		int *decpt, int ch, int *length, char *buf)
{
	int mode, dsgn;
	char *digits, *bp, *rve;
	union double_union tmp;

	tmp.d = value;
	/* This will check for "< 0" and "-0.0".  */
	if (word0 (tmp) & Sign_bit)
	{
		value = -value;
		*sign = '-';
	}
	else
		*sign = '\000';

	if (ch == 'f' || ch == 'F')
	{
		/* Ndigits after the decimal point.  */
		mode = 3;
	}
	else
	{
		/* To obtain ndigits after the decimal point for the 'e'
		   and 'E' formats, round to ndigits + 1 significant figures.  */
		if (ch == 'e' || ch == 'E')
		{
			ndigits++;
		}
		/* Ndigits significant digits.  */
		mode = 2;
	}

	digits = _DTOA_R (value, mode, ndigits, decpt, &dsgn, &rve);

	/* Print trailing zeros.  */
	if ((ch != 'g' && ch != 'G') || flags & ALT)
	{
		bp = digits + ndigits;
		if (ch == 'f' || ch == 'F')
		{
			if (*digits == '0' && value)
				*decpt = -ndigits + 1;
			bp += *decpt;
		}
		/* Kludge for __dtoa irregularity.  */
		if (value == 0)
			rve = bp;
		while (rve < bp)
			*rve++ = '0';
	}
	*length = rve - digits;
	return (digits);
}

/* This function is copied from exponent in vfprintf.c with support for
   C99 formats removed.  We don't use the original function in order to
   decouple nano implementation of formatted IO from the Newlib one.  */
	int
__exponent (char *p0, int exp, int fmtch)
{
	register char *p, *t;
	char expbuf[MAXEXPLEN];
#define isa 0

	p = p0;
	*p++ = isa ? 'p' - 'a' + fmtch : fmtch;
	if (exp < 0)
	{
		exp = -exp;
		*p++ = '-';
	}
	else
		*p++ = '+';
	t = expbuf + MAXEXPLEN;
	if (exp > 9)
	{
		do
		{
			*--t = to_char (exp % 10);
		}
		while ((exp /= 10) > 9);
		*--t = to_char (exp);
		for (; t < expbuf + MAXEXPLEN; *p++ = *t++);
	}
	else
	{
		if (!isa)
			*p++ = '0';
		*p++ = to_char (exp);
	}
	return (p - p0);
}

/* Decode and print floating point number specified by "eEfgG".  */
int _printf_float (struct _prt_data_t *pdata, va_list * ap)
{
#define _fpvalue (pdata->_double_)

	char *decimal_point = _localeconv_r (data)->decimal_point;
	size_t decp_len = strlen (decimal_point);
	/* Temporary negative sign for floats.  */
	char softsign;
	/* Integer value of exponent.  */
	int expt;
	/* Character count for expstr.  */
	int expsize = 0;
	/* Actual number of digits returned by cvt.  */
	int ndig = 0;
	char *cp;
	int n;
	/* Field size expanded by dprec(not for _printf_float).  */
	int realsz;
	char code = pdata->code;

	if (pdata->flags & LONGDBL)
	{
		_fpvalue = (double) GET_ARG (N, *ap, _LONG_DOUBLE);
	}
	else
	{
		_fpvalue = GET_ARG (N, *ap, double);
	}

	/* Do this before tricky precision changes.

	   If the output is infinite or NaN, leading
	   zeros are not permitted.  Otherwise, scanf
	   could not read what printf wrote.  */
	if (isinf (_fpvalue))
	{
		if (_fpvalue < 0)
			pdata->l_buf[0] = '-';
		if (code <= 'G')		/* 'A', 'E', 'F', or 'G'.  */
			cp = "INF";
		else
			cp = "inf";
		pdata->size = 3;
		pdata->flags &= ~ZEROPAD;
		goto print_float;
	}
	if (isnan (_fpvalue))
	{
		if (signbit (_fpvalue))
			pdata->l_buf[0] = '-';
		if (code <= 'G')		/* 'A', 'E', 'F', or 'G'.  */
			cp = "NAN";
		else
			cp = "nan";
		pdata->size = 3;
		pdata->flags &= ~ZEROPAD;
		goto print_float;
	}

	if (pdata->prec == -1)
	{
		pdata->prec = DEFPREC;
	}
	else if ((code == 'g' || code == 'G') && pdata->prec == 0)
	{
		pdata->prec = 1;
	}

	pdata->flags |= FPT;

	cp = __cvt (_fpvalue, pdata->prec, pdata->flags, &softsign,
			&expt, code, &ndig, cp);

	if (code == 'g' || code == 'G')
	{
		if (expt <= -4 || expt > pdata->prec)
			/* 'e' or 'E'.  */
			code -= 2;
		else
			code = 'g';
	}
	if (code <= 'e')
	{
		/* 'a', 'A', 'e', or 'E' fmt.  */
		--expt;
		expsize = __exponent (pdata->expstr, expt, code);
		pdata->size = expsize + ndig;
		if (ndig > 1 || pdata->flags & ALT)
			++pdata->size;
	}
	else
	{
		if (code == 'f')
		{
			/* 'f' fmt.  */
			if (expt > 0)
			{
				pdata->size = expt;
				if (pdata->prec || pdata->flags & ALT)
					pdata->size += pdata->prec + 1;
			}
			else
				/* "0.X".  */
				pdata->size = (pdata->prec || pdata->flags & ALT)
					? pdata->prec + 2 : 1;
		}
		else if (expt >= ndig)
		{
			/* Fixed g fmt.  */
			pdata->size = expt;
			if (pdata->flags & ALT)
				++pdata->size;
		}
		else
			pdata->size = ndig + (expt > 0 ? 1 : 2 - expt);
		pdata->lead = expt;
	}

	if (softsign)
		pdata->l_buf[0] = '-';
print_float:
	if (_printf_common (pdata, &realsz) == -1)
		goto error;

	if ((pdata->flags & FPT) == 0)
	{
		PRINT (cp, pdata->size);
	}
	else
	{
		/* Glue together f_p fragments.  */
		if (code >= 'f')
		{
			/* 'f' or 'g'.  */
			if (_fpvalue == 0)
			{
				/* Kludge for __dtoa irregularity.  */
				PRINT ("0", 1);
				if (expt < ndig || pdata->flags & ALT)
				{
					PRINT (decimal_point, decp_len);
					PAD (ndig - 1, pdata->zero);
				}
			}
			else if (expt <= 0)
			{
				PRINT ("0", 1);
				if (expt || ndig || pdata->flags & ALT)
				{
					PRINT (decimal_point, decp_len);
					PAD (-expt, pdata->zero);
					PRINT (cp, ndig);
				}
			}
			else
			{
				char *convbuf = cp;
				PRINTANDPAD (cp, convbuf + ndig, pdata->lead, pdata->zero);
				cp += pdata->lead;
				if (expt < ndig || pdata->flags & ALT)
					PRINT (decimal_point, decp_len);
				PRINTANDPAD (cp, convbuf + ndig, ndig - expt, pdata->zero);
			}
		}
		else
		{
			/* 'a', 'A', 'e', or 'E'.  */
			if (ndig > 1 || pdata->flags & ALT)
			{
				PRINT (cp, 1);
				cp++;
				PRINT (decimal_point, decp_len);
				if (_fpvalue)
				{
					PRINT (cp, ndig - 1);
				}
				/* "0.[0..]".  */
				else
					/* __dtoa irregularity.  */
					PAD (ndig - 1, pdata->zero);
			}
			else			/* "XeYYY".  */
				PRINT (cp, 1);
			PRINT (pdata->expstr, expsize);
		}
	}

	/* Left-adjusting padding (always blank).  */
	if (pdata->flags & LADJUST)
		PAD (pdata->width - realsz, pdata->blank);

	return (pdata->width > realsz ? pdata->width : realsz);
error:
	return -1;

#undef _fpvalue
}

#endif

