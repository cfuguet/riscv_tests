/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   io.h
 *  @author Cesar Fuguet Tortolero
 */
#ifndef __IO_H__
#define __IO_H__

#include <stdint.h>
#include "cpu.h"

static inline void iowriteb(uintptr_t addr, uint8_t val)
{
	*((volatile uint8_t*)addr) = val;
	cpu_dfence();
}

static inline void iowriteh(uintptr_t addr, uint16_t val)
{
	*((volatile uint16_t*)addr) = val;
	cpu_dfence();
}

static inline void iowritew(uintptr_t addr, uint32_t val)
{
	*((volatile uint32_t*)addr) = val;
	cpu_dfence();
}

static inline void iowritel(uintptr_t addr, uint64_t val)
{
	*((volatile uint64_t*)addr) = val;
	cpu_dfence();
}

static inline uint8_t ioreadb(uintptr_t addr)
{
	return *((volatile uint8_t*)addr);
}

static inline uint16_t ioreadh(uintptr_t addr)
{
	return *((volatile uint16_t*)addr);
}

static inline uint32_t ioreadw(uintptr_t addr)
{
	return *((volatile uint32_t*)addr);
}

static inline uint64_t ioreadl(uintptr_t addr)
{
	return *((volatile uint64_t*)addr);
}

#endif /* __IO_H__ */
