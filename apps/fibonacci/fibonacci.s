##
#  MIT License
#
#  Copyright (c) 2019 Cesar Fuguet Tortolero
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
##
##
#  @file   fibonacci.s
#  @author Cesar Fuguet Tortolero
##
.set STACK_SIZE, 0x1000
.set N, 20

.section .start,"ax",@progbits

.globl main
.align 3

main:
	# compute the Nth element of the Fibonacci's serie (recursive)
	li       a0, N
	call     fibonacci_r
	call     print_result

	# compute the Nth element of the Fibonacci's serie (iterative)
	li       a0, N
	call     fibonacci_i
	call     print_result

	# loop forever
1:
	j        1b

.globl fibonacci_r
.align 3
fibonacci_r:
# if n < 2 then return n
	sltiu    t0, a0, 2
	beq      t0, zero, 1f
	ret

1:
# else return fib(n - 2) + fib(n - 1)
	addi     sp, sp, -16
	sw       ra, 12(sp)
	sw       s0, 8(sp)
	move     s0, a0

	# fib(n - 2)
	addi     a0, s0, -2
	call     fibonacci_r
	sw       a0, 0(sp)

	# fib(n - 1)
	addi     a0, s0, -1
	call     fibonacci_r

	# fib(n - 2) + fib(n - 1)
	lw       t0, 0(sp)
	add      a0, a0, t0

# deallocate stack space and return
	lw       s0, 8(sp)
	lw       ra, 12(sp)
	addi     sp, sp, 16
	ret


.globl fibonacci_i
.align 3
fibonacci_i:
# if n < 2 then return n
	sltiu    t0, a0, 2
	bne      t0, zero, 2f

# else
	move     t0, a0      # t0 <-> n
	li       t1, 1       # t1 <-> i
	move     t2, zero    # t2 <-> prev
	li       a0, 1       # a0 <-> ret
1:
# do {
	move     t3, a0      # tmp = ret
	add      a0, a0, t2  # ret = ret + prev
	move     t2, t3      # prev = tmp
	addi     t1, t1, 1   # i++
	slt      t3, t1, t0
	bne      t3, zero, 1b
# } while (i < n)
2:
	ret


.globl print_result
.align 3
print_result:
	addi     sp, sp, -16
	sw       ra, 12(sp)
	sw       s0, 8(sp)
	move     s0, a0

	la       a0, _result_str
	call     puts
	move     a0, s0
	call     putd
	li       a0, '\n'
	call     putchar

	lw       s0, 8(sp)
	lw       ra, 12(sp)
	addi     sp, sp, 16
	ret

.section .rodata

.align 2

_result_str:
	.asciz   "result: "
