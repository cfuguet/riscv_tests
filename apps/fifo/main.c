/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   main.c
 *  @date   October 2019
 *  @author Cesar Fuguet Tortolero
 */
#include <stdint.h>
#include <stdio.h>
#include <barrier.h>
#include <cpu.h>
#include <ticket_lock.h>

#define FIFO_NELEMS 512

typedef struct fifo_s {
	void *buffer[FIFO_NELEMS];
	int rptr;
	int wptr;

	ticket_lock_t lock;
} fifo_t;

int fifo_init(fifo_t *fifo)
{
	ticket_lock_init(&fifo->lock);
	cpu_atomic_store(&fifo->rptr, 0);
	cpu_atomic_store(&fifo->wptr, 0);
}

int fifo_is_empty(fifo_t *fifo)
{
	int rptr = cpu_atomic_load(&fifo->rptr);
	int wptr = cpu_atomic_load(&fifo->wptr);

	return rptr == wptr;
}

int fifo_is_full(fifo_t *fifo)
{
	int rptr = cpu_atomic_load(&fifo->rptr);
	int wptr = cpu_atomic_load(&fifo->wptr);
	int wnxt = (wptr + 1) % FIFO_NELEMS;

	return rptr == wnxt;
}

int fifo_fill_state(fifo_t *fifo)
{
	int rptr = cpu_atomic_load(&fifo->rptr);
	int wptr = cpu_atomic_load(&fifo->wptr);

	if (wptr >= rptr)
		return (wptr - rptr);
	else
		return (FIFO_NELEMS + wptr) - rptr;
}

void fifo_write(fifo_t *fifo, void *val)
{
	int wptr;

	for (;;) {
		while (fifo_is_full(fifo));

		ticket_lock_acquire(&fifo->lock);
		if (fifo_is_full(fifo))
			ticket_lock_release(&fifo->lock);
		else
			break;
	}

	wptr = cpu_atomic_load(&fifo->wptr);
	cpu_atomic_store(&fifo->wptr, (wptr + 1) % FIFO_NELEMS);
	fifo->buffer[wptr] = val;

	ticket_lock_release(&fifo->lock);
}

void* fifo_read(fifo_t *fifo)
{
	int rptr;
	void *ret;

	for (;;) {
		while (fifo_is_empty(fifo));

		ticket_lock_acquire(&fifo->lock);
		if (fifo_is_empty(fifo))
			ticket_lock_release(&fifo->lock);
		else
			break;
	}

	rptr = cpu_atomic_load(&fifo->rptr);
	cpu_atomic_store(&fifo->rptr, (rptr + 1) % FIFO_NELEMS);
	ret = fifo->buffer[rptr];

	ticket_lock_release(&fifo->lock);
	return ret;
}
