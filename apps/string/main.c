/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   main.c
 *  @author Cesar Fuguet Tortolero
 */
#include <stdio.h>
#include <cpu.h>
#include <string.h>

char test_str[] = "This is a test string";

void secondary()
{
	/* secondary processor do nothing */
	for (;;);
}

void main()
{
	char str[256];
	char *s0 = "This";
	char *s1 = "is";
	char *s2 = "a";
	char *s3 = "test...";

	/* copy the empty string (initialize to empty) */
	strcpy(str, "");

	/* concatenate strings */
	strcat(str, s0);
	strcat(str, " ");
	strcat(str, s1);
	strcat(str, " ");
	strcat(str, s2);
	strcat(str, " ");
	strncat(str, s3, 4);

	/* test zero-length concatenation */
	strncat(str, s3, 0);
	strcat(str, "");

	/* print the resulting string */
	printf("%s\n", str);

	/* print the size of the resulting string */
	printf("Size of the result string: %d\n", strlen(str));

	/* check that it is correct */
	if (strcmp(str, test_str) == 0)
		puts("The result string is correct\n");
	else
		puts("The result string is wrong\n");

	/* check self-comparison */
	if (strcmp(str, str) == 0)
		puts("Self-comparison passed\n");
	else
		puts("Self-comparison failed\n");

	/* check partial comparison */
	if (strncmp(str, test_str, 5) == 0)
		puts("Partial comparison passed\n");
	else
		puts("Partial comparison failed\n");

	/* check against different string */
	int ret;
	if ((ret = strcmp(str, "This is a different string")) != 0) {
		if (ret != 16)  {
			char msg[] =
				"error: strings are correctly determined as "
				"different, but the lexicographical "
				"difference value is incorrect";
			puts(msg);
		} else {
			puts("Different comparison passed\n");
		}
	} else {
		puts("Different comparison failed\n");
	}

	puts("Test finished\n");
}
