/**
 *  @author    Cesar Fuguet Tortolero
 *  @date      December 2018
 *  @brief     Display Manipulation Functions
 */
#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdint.h>

static inline uint32_t convert_rgb565_rgb888(uint16_t pixel)
{
	uint32_t r, g, b;
	r = ((pixel >> 11) & 0x1F) << 3;
	g = ((pixel >> 5)  & 0x3F) << 2;
	b = ((pixel >> 0)  & 0x1F) << 3;
	//return (r << 16) | (g << 8)| b;
	return (0xffUL << 24) | (b << 16) | (g << 8)| r;
}

#endif /* __DISPLAY_H__ */

/* vim: tabstop=5 : softtabstop=4 : shiftwidth=4 : noexpandtab : list
 */
