/**
 *  @author    Cesar Fuguet Tortolero
 *  @date      December 2018
 *  @brief     Mandelbrot's Fractal Computation and Display
 */
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <cpu.h>
#include <io.h>
#include "mandelbrot.h"

#ifndef ENABLE_DISPLAY
#define ENABLE_DISPLAY 1
#endif

#if ENABLE_DISPLAY
#include "display.h"
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#define PARAMS_ITERS       (1UL << 8)
#define PARAMS_WIDTH       256
#define PARAMS_HEIGHT      256
#define PARAMS_OFFX        -0.75f
#define PARAMS_OFFY        0.0
#define PARAMS_ZOOM        0.8

#define PARAMS_MAXNTHREADS 16
#define PARAMS_BLOCKSIZE   4

extern uintptr_t _cpu_boot_vector[16];

static unsigned int line_pool;
static unsigned int barrier;
static unsigned int nthreads;

static inline void mandelbrot_line(uint32_t *pixels, unsigned int y)
{
	const unsigned int iters = PARAMS_ITERS;
	const unsigned int h = PARAMS_HEIGHT;
	const unsigned int w = PARAMS_WIDTH;
	const float z = PARAMS_ZOOM;
	const float p_i = PARAMS_OFFY + ((y - h/2.0f)/(0.5f*z*h));
	unsigned int x;

	for (x = 0; x < w; ++x) {
		float p_r = PARAMS_OFFX + 1.5f*((x - w/2.0f)/(0.5f*z*w));
		unsigned int i = mandelbrot_compute(iters, p_r, p_i);
		pixels[x] = convert_rgb565_rgb888(mandelbrot_color(i));
	}
}

static inline unsigned int mandelbrot_getlineblock(int blocksize)
{
	if (cpu_atomic_load((int*)&line_pool) >= PARAMS_HEIGHT)
		return PARAMS_HEIGHT;

	return cpu_atomic_fetch_add((int*)&line_pool, blocksize);
}

static inline void display_clear(int line, int nlines)
{
	const int line_size = PARAMS_WIDTH*sizeof(int);
	uintptr_t display_offset = 0xc3000000UL + line*line_size;
	memset((void*)display_offset, 0x0, nlines*line_size);
}

static inline void display_draw(uint32_t *pixels, int line, int nlines)
{
	const int line_size = PARAMS_WIDTH*sizeof(int);
	uintptr_t display_offset = 0xc3000000UL + line*line_size;
	memcpy((void*)display_offset, pixels, nlines*line_size);
}

void mandelbrot()
{
	uint32_t pixels[PARAMS_BLOCKSIZE][PARAMS_WIDTH];
	unsigned int line;

	cpu_atomic_fetch_add((int*)&nthreads, 1);
	putchar('+');

	for (   line = mandelbrot_getlineblock(PARAMS_BLOCKSIZE);
			line < PARAMS_HEIGHT;
			line = mandelbrot_getlineblock(PARAMS_BLOCKSIZE))
	{
		unsigned int i;

		for (i = 0; i < PARAMS_BLOCKSIZE; ++i)
			mandelbrot_line(pixels[i], line++);

#if ENABLE_DISPLAY
		display_draw((uint32_t*)pixels,
				line - PARAMS_BLOCKSIZE,
				PARAMS_BLOCKSIZE);
#endif
	}

	/* the thread increments the barrier to signal that it finished */
	cpu_atomic_fetch_add((int*)&barrier, 1);
}

void main()
{
	int i;

	/* initialize line pool */
	cpu_atomic_store((int*)&line_pool, 0);

	/* initialize barrier */
	cpu_atomic_store((int*)&barrier, 0);

	/* initialize nthreads */
	cpu_atomic_store((int*)&nthreads, 0);

	/* clear the display */
	display_clear(0, PARAMS_HEIGHT);

	puts("Mandelbrot's fractal computation started\n");

	/* start worker threads */
	for (i = 1; i < PARAMS_MAXNTHREADS; i++)
		cpu_atomic_store((int*)&_cpu_boot_vector[i], (intptr_t)mandelbrot);

	/* FIXME get the start time */

	/* main thread does some work too :) */
	mandelbrot();

	/* wait for all threads to finish */
	while (cpu_atomic_load((int*)&barrier) != cpu_atomic_load((int*)&nthreads))
		cpu_delay(1 << 15);

	/* FIXME get the end time */

	puts("\nMandelbrot's fractal computation completed\n");

	/* FIXME statistics display */
}

/* vim: tabstop=4 : softtabstop=4 : shiftwidth=4 : noexpandtab : list
*/
