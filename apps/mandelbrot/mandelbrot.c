#include "mandelbrot.h"

/*
 *  compute a mandelbrot's pixel
 */
unsigned int mandelbrot_compute(unsigned int iters, float p_r, float p_i)
{
	float new_r = 0, new_i = 0, old_r = 0, old_i = 0;
	unsigned int i = 0;
	while (((new_r*new_r + new_i*new_i) < 4.0f) && (i < iters)) {
		old_r = new_r;
		old_i = new_i;
		new_r = old_r*old_r - old_i*old_i + p_r;
		new_i = 2.0f*old_r*old_i + p_i;
		++i;
	}
	return i;
}

/* vim: tabstop=4 : softtabstop=4 : shiftwidth=4 : noexpandtab : list
 */
