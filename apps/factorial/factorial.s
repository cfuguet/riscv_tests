##
#  MIT License
#
#  Copyright (c) 2019 Cesar Fuguet Tortolero
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
##
##
#  @file   factorial.s
#  @author Cesar Fuguet Tortolero
##
.set STACK_SIZE, 0x1000
.set N, 5

.section .start,"ax",@progbits

.globl main
.align 3

main:
	# compute the factorial of N (recursive)
	li       a0, N
	call     factorial_r
	call     print_result

	# compute the factorial of N (iterative)
	li       a0, N
	call     factorial_i
	call     print_result

	# loop forever
1:
	j        1b

.globl factorial_r
.align 3
factorial_r:
## if n < 2 then return 1
	slti     t0, a0, 2
	beq      t0, zero, 1f
	addi     a0, zero, 1  # return 1
	ret

1:
## else return n*factorial(n - 1)
	addi     sp, sp, -16
	sw       ra, 12(sp)
	sw       s0, 8(sp)
	addi     s0, a0, 0

	# factorial(n - 1)
	addi     a0, s0, -1
	call     factorial_r

	# n*factorial(n - 1)
	mul      a0, a0, s0

# deallocate stack space and return
	lw       s0, 8(sp)
	lw       ra, 12(sp)
	addi     sp, sp, 16
	ret


.globl factorial_i
.align 3
factorial_i:
	addi     t0, a0, 1   # t0 <-> n + 1
	addi     t1, zero, 1 # t1 <-> i
	addi     a0, zero, 1 # a0 <-> ret

# for (i = 1; i < (n + 1); i++) {
1:
	slt      t2, t1, t0
	beq      t2, zero, 2f
	mul      a0, a0, t1 # ret = ret * i
	addi     t1, t1, 1
	j        1b
# }
2:
	ret


.globl print_result
.align 3
print_result:
	addi     sp, sp, -16
	sw       ra, 12(sp)
	sw       s0, 8(sp)
	addi     s0, a0, 0

	la       a0, _result_str
	call     puts
	addi     a0, s0, 0
	call     putd
	li       a0, '\n'
	call     putchar

	lw       s0, 8(sp)
	lw       ra, 12(sp)
	addi     sp, sp, 16
	ret


.section .rodata

.align 3

_result_str:
	.asciz   "result: "
