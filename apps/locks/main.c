/**
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
/**
 *  @file   main.c
 *  @author Cesar Fuguet Tortolero
 */
#include <stdint.h>
#include <stdio.h>
#include <barrier.h>
#include <cpu.h>
#include <lock.h>
#include <ticket_lock.h>

#define PARAMS_NTHREADS 16

lock_t simple_lock;
ticket_lock_t ticket_lock;
barrier_t barrier;
uint64_t simple_cycles;
uint64_t ticket_cycles;

typedef struct stats_s {
	uint64_t simple_cycles;
	uint64_t ticket_cycles;
} stats_t;

static stats_t parallel_test_lock()
{
	int i;
	uint64_t start;
	stats_t stats;

	/* take the simple lock */
	start = cpu_cycles();
	for (i = 0; i < 16; i++) {
		lock_acquire(&simple_lock);
		lock_release(&simple_lock);
	}
	stats.simple_cycles = cpu_cycles() - start;

	barrier_wait(&barrier);

	/* take the ticket lock */
	start = cpu_cycles();
	for (i = 0; i < 16; i++) {
		ticket_lock_acquire(&ticket_lock);
		ticket_lock_release(&ticket_lock);
	}
	stats.ticket_cycles = cpu_cycles() - start;

	return stats;
}

void parallel_worker()
{
	stats_t stats;

	/* wait for all threads to be ready */
	barrier_wait(&barrier);

	stats = parallel_test_lock();

	ticket_lock_acquire(&ticket_lock);
	simple_cycles += stats.simple_cycles;
	ticket_cycles += stats.ticket_cycles;
	ticket_lock_release(&ticket_lock);

	/* wait for all threads to finish */
	barrier_wait(&barrier);
}

void main()
{
	int i;

	puts("Test started\n");

	lock_init(&simple_lock);
	ticket_lock_init(&ticket_lock);
	barrier_init(&barrier, PARAMS_NTHREADS);

	/* start worker threads */
	for (i = 1; i < PARAMS_NTHREADS; i++) {
		extern uintptr_t _cpu_boot_vector[16];
		int *cpu_entry = (int*)&_cpu_boot_vector[i];
		cpu_atomic_store(cpu_entry, (intptr_t)parallel_worker);
	}

	/* start test procedure */
	parallel_worker();

	puts("Simple lock took ");
	putu((unsigned)simple_cycles/PARAMS_NTHREADS);
	puts(" cycles\n");

	puts("Ticket lock took ");
	putu((unsigned)ticket_cycles/PARAMS_NTHREADS);
	puts(" cycles\n");

	puts("Test finished\n");
}
