#!/bin/bash
##
# Author:
#   Cesar Fuguet Tortolero
#
# Description:
#   This script allows generating a tag file. A tag file is an indexing-file
#   allowing to browse for objects (variables, functions, macros, types...) in
#   a program. You need a text editor supporting the browsing with a tag file
#   like Vim or Emacs.
##
COMMANDS="cscope ctags"
SOURCE_DIRS="common platform test_*"

# look for supported tools in the system
gentags=""
for c in ${COMMANDS} ; do
	command=$(command -v $c)
	case ${command} in
		*cscope)
			gentags="${command} -Rb" ;
			break ;
			;;
		*ctags)
			gentags="${command} -dt" ;
			break ;
			;;
	esac
done

if [ -z "${gentags}" ] ; then
	printf "error: no supported indexing tool found\n" ;
	exit 1 ;
fi

# look for .c and .h files
files=$(find ${SOURCE_DIRS} \( -name '*.c' -or -name '*.h' \) -print)

# create a string with all files (removing newline characters)
args="$(echo -E "${files}" | tr '\n' ' ')"

# execute the tag generation tool
${gentags} ${args}

exit 0
